package com.example.universe

import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var data: Array<Planet> = arrayOf(
                Planet(getString(R.string.mercury), BitmapFactory.decodeResource(resources,R.drawable.mercury),
                        "57.91 "+getString(R.string.million)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.mercury_web)) ,
                Planet(getString(R.string.venus),BitmapFactory.decodeResource(resources,R.drawable.venera),
                        "108 "+getString(R.string.million)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.venus_web)),
                Planet(getString(R.string.earth),BitmapFactory.decodeResource(resources,R.drawable.earth),
                        "149.6 "+getString(R.string.million)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.life),
                        getString(R.string.wiki_web)+getString(R.string.earth_web)),
                Planet(getString(R.string.mars),BitmapFactory.decodeResource(resources,R.drawable.mars),
                        "228 "+getString(R.string.million)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.mars_web)),
                Planet(getString(R.string.jupiter),BitmapFactory.decodeResource(resources,R.drawable.jupiter),
                        "778.57 "+getString(R.string.million)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.jupiter_web)),
                Planet(getString(R.string.saturne),BitmapFactory.decodeResource(resources,R.drawable.saturne),
                        "1430 "+getString(R.string.million)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.saturne_web)),
                Planet(getString(R.string.uranus),BitmapFactory.decodeResource(resources,R.drawable.uran),
                        "2.8 "+getString(R.string.billion)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.uran_web)),
                Planet(getString(R.string.neptune),BitmapFactory.decodeResource(resources,R.drawable.neptune),
                        "4,55 "+getString(R.string.billion)+" "+getString(R.string.km)+" "+getString(R.string.from_the_sun),
                        BitmapFactory.decodeResource(resources,R.drawable.no_life),
                        getString(R.string.wiki_web)+getString(R.string.neptune_web))
                )
        list.layoutManager =LinearLayoutManager(applicationContext)
        list.adapter= PlanetAdapter(data)
    }

}
