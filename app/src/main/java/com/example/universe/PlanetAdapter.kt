package com.example.universe

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.item.view.*

class PlanetAdapter (var data:Array<Planet>): RecyclerView.Adapter<PlanetAdapter.PlanetViewHolder>() {
    override fun onBindViewHolder(holder: PlanetViewHolder, position: Int) {
        holder.nameText.text = data[position].name
        holder.img.setImageBitmap(data[position].img)
        holder.length.text =data[position].length
        holder.life.setImageBitmap(data[position].life)
        holder.web.text = data[position].webSite

        holder.itemView.setOnClickListener({
            var intent =Intent(holder.itemView.context, Web::class.java)
            intent.putExtra(Intent.EXTRA_TEXT, data[position].webSite)
            holder.itemView.context.startActivity(intent)})
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            PlanetViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item,parent,false))

    override fun getItemCount() = data.size //Кол-во записей

    class PlanetViewHolder(view: View):RecyclerView.ViewHolder(view){
        var img: ImageView = view.item_image
        var nameText: TextView = view.item_name
        var length: TextView = view.item_length
        var life: ImageView = view.item_life
        var web: TextView = view.item_web

    }
}